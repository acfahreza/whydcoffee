
import { createStackNavigator } from '@react-navigation/stack';
import { SplashScreen, LoginScreen, RegisterScreen, WelcomeAuth, HomeScreen, UserScreen } from '../../containers/pages';
import 'react-native-gesture-handler';
import * as React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createStackNavigator();

const Router = () => {
    return (
        //<Stack.Navigator initialRouteName="Home Screen">
        <Stack.Navigator>
            <Stack.Screen name="SplashScreen" component={SplashScreen} options={{
                headerTransparent: true,
                headerTitleAlign: 'center',
            }} />
            <Stack.Screen name="WelcomeAuth" component={WelcomeAuth} options={{
                headerTransparent: true,
                headerShown: false,
                headerTintColor: 'white',
                headerTitleAlign: 'center',
                headerStyle: { backgroundColor: '#00BCD4', }
            }} />

            <Stack.Screen name="Login Screen" component={LoginScreen} options={{
                headerTransparent: true,
                headerShown: true,
                headerTintColor: 'black',
                headerTitleAlign: 'center',
                headerStyle: { backgroundColor: '#00BCD4', }
            }} />
            <Stack.Screen name="Register Screen" component={RegisterScreen} options={{
                headerTransparent: true,
                headerShown: true,
                headerTintColor: 'black',
                headerTitleAlign: 'center',
                headerStyle: { backgroundColor: '#00BCD4', }
            }} />
            <Stack.Screen name="Home Screen" component={HomeScreen} options={{
                headerShown: false,
                headerTintColor: 'black',
                headerTitleAlign: 'center',
                headerStyle: { backgroundColor: '#00BCD4', }
            }} />
            <Stack.Screen name="User Screen" component={UserScreen} options={{
                headerTransparent: true,
                headerShown: true,
                headerTintColor: 'black',
                headerTitleAlign: 'center',
                headerStyle: { backgroundColor: '#00BCD4', }
            }} />

        </Stack.Navigator>


    )
}

export default Router;