import React, { useState } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { colors } from '../../../utils';

const Input = ({ placeholder, ...rest }) => {
    const [form, setForm] = useState({
        fullName: '',
        email: '',
        passwaord: '',
    });
    myfun = async () => {
        alert(form);
    }
    return (

        <TextInput style={styles.input}
            placeholder={placeholder}
            placeholderTextColor={colors.default}
            {...rest} />

    );
}
const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        fontSize: 20,
        borderColor: colors.default,
        borderRadius: 25,
        paddingVertical: 12,
        paddingHorizontal: 18,
        fontSize: 14,
        color: colors.default
    }
});
export default Input;


