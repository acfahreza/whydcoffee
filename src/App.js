import React, { useState } from 'react';
import { StatusBar, StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Router from "./config/router";
import { NavigationContainer } from '@react-navigation/native';

const App = () => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  myfun = async () => {
    alert(email);

    // await fetch('http://', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({ "email": email, "password": password })
    // }).then(res => res.json())
    //   .then(resData => {
    //     alert(resData.message);
    //     console.log(resData);
    //   });

  }
  return (
    <NavigationContainer style={{ backgroundColor: '#00BCD4' }}>
      <View style={{ backgroundColor: '#00BCD4', flex: 1 }}>
        <View
          //To set the background color in IOS Status Bar also
          style={{
            backgroundColor: '#00BCD4',
            height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
          }}></View>

        <StatusBar
          barStyle="dark-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#00BCD4"
          //Background color of statusBar only works for Android
          translucent={true}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <Router />
      </View>
    </NavigationContainer>
  );
};


export default App;
