import SplashScreen from './SplashScreen';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import HomeScreen from './HomeScreen';
import UserScreen from './UserScreen';
import WelcomeAuth from './WelcomeAuth';


export {
    SplashScreen, LoginScreen, RegisterScreen, WelcomeAuth, HomeScreen, UserScreen
};

