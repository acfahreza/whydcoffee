import React, { useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Button } from '../../../components';
import ActionButton from './../WelcomeAuth/ActionButton';

const HomeScreen = ({ navigation }) => {
    const handleGoTo = screen => {
        navigation.navigate(screen);
    }
    const [form, setForm] = useState({
        fullName: '',
        email: '',
        password: '',
    });

    const sendData = () => {
        console.log('data yang dikirim', form);
        // mengirim data
        // axios.post('url', form);
    }

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#00BCD4' }}>
            <Text style={{ textAlign: 'center', fontSize: 20, marginTop: -5 }}>HomeScreen</Text>
            <ScrollView>
                <View>
                    <View style={{ paddingTop: 16, paddingHorizontal: 16 }}>
                        <View style={{ position: 'relative' }}>

                            <View style={{ width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, backgroundColor: 'black', opacity: 0.15, borderRadius: 6 }}></View>
                            <View style={{ height: 15, width: 60, position: 'absolute', top: 16, left: 16 }}>

                            </View>
                        </View>
                        <View style={{ paddingTop: 16, paddingBottom: 20, borderBottomColor: '#E8E9ED', borderBottomWidth: 0 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#1C1C1C' }}>GO-NEWS</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'normal', color: '#7A7A7A', marginBottom: 11 }}>Dimas Drajat Selamatkan Pinalti, Timnas U-23 Kalahkan Brunei .</Text>
                            <TouchableOpacity style={{ backgroundColor: '#61A756', paddingHorizontal: 12, paddingVertical: 11, alignSelf: 'flex-end', borderRadius: 4, textAlign: 'center' }}>
                                <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'white' }}>READ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </ScrollView>

            <View>
                <View style={{ margin: 10, alignItems: 'center' }}>
                    <ActionButton title="User" onPress={() => handleGoTo('User Screen')} />
                </View>
            </View>


        </View>
    );
}
export default HomeScreen;
