import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

const LoginScreen = () => {
    const [form, setForm] = useState({
        fullName: '',
        email: '',
        passwaord: '',
    });
    myfun = async () => {
        alert([form]);
    }
    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    }
    return (

        <View style={{ backgroundColor: '#00BCD4', flex: 1 }}>
            <TextInput placeholder="Email"
                style={{
                    borderWidth: 1,
                    borderColor: 'blue',
                    margin: 10, marginTop: 190
                }} value={form.email} onChangeText={(value) => onInputChange(value, 'email')} />

            <TextInput placeholder="Password" style={styles.input} value={form.password} onChangeText={(value) => onInputChange(value, 'password')} />
            <View>
                <TouchableOpacity onPress={myfun}>
                    <Text style={styles.btn}>Login</Text>
                </TouchableOpacity>
            </View>

        </View>

    )
}

const styles = StyleSheet.create({

    heading: {
        textAlign: 'center', fontSize: 20, margin: 10
    },
    input: {
        borderWidth: 1,
        borderColor: 'blue',
        margin: 10

    },
    btn: {
        backgroundColor: 'blue',
        color: '#ffff',
        fontSize: 22,
        textAlign: 'center',
        padding: 13,
        margin: 10

    },

    body: {
        backgroundColor: '#00BCD4',
    },
});
export default LoginScreen;

