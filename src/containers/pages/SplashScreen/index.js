import React, { useEffect } from 'react';
import { Text, View } from 'react-native';

const SplashScreen = ({ navigation }) => {
    useEffect(
        () => {
            setTimeout(
                () => {
                    navigation.replace('WelcomeAuth');
                }, 2000);
        }
    );
    return (
        <>
            <View style={{ backgroundColor: '#00BCD4', flex: 1 }}>

            </View>
        </>
    )
}

export default SplashScreen;

