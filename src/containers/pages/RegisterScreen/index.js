import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Input, Button } from '../../../components';
import { ScrollView } from 'react-native-gesture-handler';
// import { Kback } from '../../../assets';

const RegisterScreen = () => {
    const [form, setForm] = useState({
        fullName: '',
        email: '',
        password: '',
    });

    const sendData = () => {
        console.log('data yang dikirim', form);
        // mengirim data
        // axios.post('url', form);
    }

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    }
    return (

        <View style={{ backgroundColor: '#00BCD4', flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ marginTop: 90, margin: 5 }}>
                    <View style={{ marginTop: 90 }}>
                        {/* <Kback width={25} height={25} /> */}
                    </View>
                    <View style={styles.space(64)}>
                        <Input placeholder="Email" value={form.email} onChangeText={(value) => onInputChange(value, 'email')} />
                    </View>
                    <View style={styles.space(64)}>
                        <Input placeholder="Name" value={form.fullName} onChangeText={(value) => onInputChange(value, 'fullName')} />
                    </View>
                    <View style={styles.space(64)}>
                        <Input placeholder="Password" value={form.password} onChangeText={(value) => onInputChange(value, 'password')} secureTextEntry={true} />
                    </View>
                    <View>
                        <Button title="Daftar" onPress={sendData} />
                    </View>
                </View>
            </ScrollView>
        </View>

    )
}

const styles = StyleSheet.create({

    heading: {
        textAlign: 'center', fontSize: 20, margin: 10
    },
    input: {
        borderWidth: 1,
        borderColor: 'blue',
        margin: 10

    },
    space: (value) => {
        return {
            height: value,
            marginBottom: 10
        }
    },
    btn: {
        backgroundColor: 'blue',
        color: '#ffff',
        fontSize: 22,
        textAlign: 'center',
        padding: 13,
        margin: 10

    },

    body: {
        backgroundColor: '#00BCD4',
    },
});
export default RegisterScreen;

