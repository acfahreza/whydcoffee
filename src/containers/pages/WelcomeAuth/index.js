import React from 'react';

import { Text, View, Image } from 'react-native';
import ActionButton from './ActionButton';
import { colors } from '../../../utils';
import { welcomeAuth } from './../../../assets';

const WelcomeAuth = ({ navigation }) => {
    const handleGoTo = screen => {
        navigation.navigate(screen);
    }
    return (
        <View style={styles.wrapper.page}>
            <View>
                <Image source={welcomeAuth} style={styles.wrapper.illustration} />
                <Text style={styles.text.welcome}> Auth Screen</Text>
                <ActionButton title="Login" onPress={() => handleGoTo('Login Screen')} />
                <ActionButton title="Register" onPress={() => handleGoTo('Register Screen')} />
                <ActionButton title="Home" onPress={() => handleGoTo('Home Screen')} />
                <ActionButton title="User" onPress={() => handleGoTo('User Screen')} />
            </View>
        </View>

    )
}

const styles = {
    wrapper: {
        page: {
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#00BCD4',
            flex: 1,
        },
        illustration: {
            width: 219,
            height: 117,
            backgroundColor: colors.default,
            marginBottom: 10
        },
    },
    text: {
        welcome: {
            fontSize: 18,
            fontWeight: 'bold',
            marginBottom: 20
        }
    },
}

export default WelcomeAuth;

